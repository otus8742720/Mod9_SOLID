﻿namespace Mod9_SOLID.Abstractions;

internal interface IGuessGame
{
    public void Play();
}
