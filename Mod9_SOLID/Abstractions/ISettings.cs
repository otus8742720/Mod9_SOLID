﻿namespace Mod9_SOLID.Abstractions;

internal interface ISettings
{
    public int MinValue { get; }
    public int MaxValue { get; }
    public int MaxShots { get; set; }
}
