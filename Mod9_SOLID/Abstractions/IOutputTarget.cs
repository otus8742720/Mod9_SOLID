﻿namespace Mod9_SOLID.Abstractions;

internal interface IOutputTarget
{
    void ShowMessage(string message);
}
