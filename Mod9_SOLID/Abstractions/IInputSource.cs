﻿namespace Mod9_SOLID.Abstractions;

internal interface IInputSource
{
    int GetInteger(string message = "Введите целое число: ");
}
