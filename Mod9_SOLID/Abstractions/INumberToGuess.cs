﻿namespace Mod9_SOLID.Abstractions;

internal interface INumberToGuess
{
    public int MinValue { get; }
    public int MaxValue { get; }
    public int MaxShots { get; set; }
    public int CorrectNumber { get; set; }
}
