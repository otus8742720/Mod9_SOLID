﻿namespace Mod9_SOLID;

internal static class NonSolidVersion
{
    internal static void GuessNumber()
    {
        int number = new Random().Next(0, 10);
        Console.Write("Угадайте число: ");
        int userGuess;
        int MaxShots = 5;
        int currentShot = 0;
        do
        {
            currentShot++;
            if (!int.TryParse(Console.ReadLine(), out userGuess))
            {
                Console.Write("Ожидается целое число. Попробуйте еще раз: ");
                continue;
            }

            if (number > userGuess)
            {
                Console.WriteLine("маловато");
            }
            else if (number < userGuess)
            {
                Console.WriteLine("многовато");
            }
            else
            {
                Console.WriteLine("Правильно!");
                break;
            }

        } while (currentShot < MaxShots);

        Console.WriteLine("Game Over");
        Console.ReadLine();
    }
}
