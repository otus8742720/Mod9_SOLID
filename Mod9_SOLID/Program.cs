﻿using Microsoft.Extensions.Configuration;
using Mod9_SOLID.Domain;
using Mod9_SOLID.Services;
using Mod9_SOLID.Abstractions;

namespace Mod9_SOLID;

internal class Program
{
    static void Main(string[] args)
    {
        IConfigurationBuilder builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false);
       
        IConfiguration config = builder.Build();
        ISettings settings = new Settings(config);

        IOutputTarget outputTarget = new DefaultOutputTarget();
        IInputSource inputSource = new DefaultInputSource(outputTarget);
        INumberToGuess service = new NumberToGuess(settings);
        IGuessGame guessGame = new GuessGame(service, inputSource, outputTarget);
        guessGame.Play();
        Console.ReadLine();
    }
}