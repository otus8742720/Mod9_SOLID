﻿using Mod9_SOLID.Abstractions;

namespace Mod9_SOLID.Services;

internal class DefaultOutputTarget : IOutputTarget
{
    public void ShowMessage(string message)
    {
        Console.WriteLine(message);
    }
}
