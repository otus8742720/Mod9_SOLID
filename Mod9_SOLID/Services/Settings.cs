﻿using Microsoft.Extensions.Configuration;
using Mod9_SOLID.Abstractions;

namespace Mod9_SOLID.Services;

internal class Settings : ISettings
{
    public int MinValue { get; }
    public int MaxValue { get; }
    public int MaxShots { get; set; }

    public Settings(IConfiguration config)
    {
        try
        {
            MinValue = Int32.Parse(config["MinValue"]);
            MaxValue = Int32.Parse(config["MaxValue"]);
            MaxShots = Int32.Parse(config["MaxShots"]);
        }
        catch (Exception ex)
        {
            //Нужно бы как-то проинформировать об ошибке чтения конфига?
            MinValue = 0;
            MaxValue = 10;
            MaxShots = 5;
        }
    }
}
