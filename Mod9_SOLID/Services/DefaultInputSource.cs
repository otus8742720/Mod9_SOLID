﻿using Mod9_SOLID.Abstractions;

namespace Mod9_SOLID.Services;

internal class DefaultInputSource : IInputSource
{
    readonly IOutputTarget outputTarget;
    public DefaultInputSource(IOutputTarget target)
    {
        outputTarget = target;
    }
    public int GetInteger(string message = "Введите целое число: ")
    {
        outputTarget.ShowMessage(message);
        if (int.TryParse(Console.ReadLine(), out int value))
        {
            return value;
        }
        return GetInteger();
    }
}
