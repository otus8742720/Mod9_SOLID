﻿using Mod9_SOLID.Abstractions;

namespace Mod9_SOLID.Services;

internal class GuessGame : IGuessGame
{
    INumberToGuess _numberToGuess;
    IInputSource _inputSource;
    IOutputTarget _outputTarget;
    public GuessGame(INumberToGuess numberToGuess, IInputSource inputSource, IOutputTarget outputTarget)
    {
        _numberToGuess = numberToGuess;
        _inputSource = inputSource;
        _outputTarget = outputTarget;
    }

    public void Play()
    {
        int currentInput;
        int currentShot = 0;

        _outputTarget.ShowMessage($"Угадайте число в диапазоне от {_numberToGuess.MinValue} до {_numberToGuess.MaxValue}. Есть {_numberToGuess.MaxShots} попыток.");

        do
        {
            currentShot++;
            currentInput = _inputSource.GetInteger();
            if (currentInput < _numberToGuess.CorrectNumber)
            {
                _outputTarget.ShowMessage("маловато");
            }
            else if (currentInput > _numberToGuess.CorrectNumber)
            {
                _outputTarget.ShowMessage("многовато");
            }
            else
            {
                _outputTarget.ShowMessage("Правильно!");
                break;
            }

        } while (currentShot <= _numberToGuess.MaxShots);

        _outputTarget.ShowMessage("-- Конец игры --");
    }
}

