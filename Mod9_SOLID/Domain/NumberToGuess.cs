﻿using Mod9_SOLID.Abstractions;

namespace Mod9_SOLID.Domain;

internal class NumberToGuess : INumberToGuess
{
    public int MinValue { get; }
    public int MaxValue { get; }
    public int MaxShots { get; set; }
    public int CorrectNumber { get; set; }
    public NumberToGuess(ISettings settings)
    {
        MinValue = settings.MinValue;
        MaxValue = settings.MaxValue;
        MaxShots = settings.MaxShots;
        CorrectNumber = new Random().Next(settings.MinValue, settings.MaxValue);
    }
}


